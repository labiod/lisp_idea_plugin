package com.kgb.lisp.icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * Created by labiod : <labiod@wp.pl>
 * Class com.kgb.lisp.icons.LispIcons
 */
public class LispIcons {
    public static final Icon ICON = IconLoader.getIcon("/com/kgb/lisp/icons/lisp-icon.png");
}
